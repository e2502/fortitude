fortitude_global_empire_doing_okay = {
	num_of_electors = 6 #it can be fine for there to be only 6 electors temporarily while the emperor appoints a new one, but having fewer than 6 suggests serious issues
	variable_arithmetic_trigger = { #the IA change from (HRE size and foreign provinces) alone is not worse than -0.075
		export_to_variable = {
			which = comparable_hre_size
			value = trigger_value:hre_size
		}
		export_to_variable = {
			which = comparable_num_of_foreign_hre_provinces
			value = trigger_value:num_of_foreign_hre_provinces
		}
		multiply_variable = {
			which = comparable_hre_size
			value = 0.003
		}
		multiply_variable = {
			which = comparable_num_of_foreign_hre_provinces
			value = 0.005
		}
		check_variable = {
			which = comparable_hre_size
			which = comparable_num_of_foreign_hre_provinces
		}
	}
}

fortitude_global_any_system_active = {
	OR = {
		has_global_flag = fortitude_settings_system_forts
		has_global_flag = fortitude_settings_system_buildings
		has_global_flag = fortitude_settings_system_reposition_colonists
		has_global_flag = fortitude_settings_system_abandon_ideas
	}
}
