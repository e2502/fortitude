namespace = fortitude_events_settings

#1. Initialize
#2. Settings

country_event = {
	id = fortitude_events_settings.1
	title = fortitude_events_settings.1.t
	desc = fortitude_events_settings.1.d
	picture = BATTLE_eventPicture
	hidden = yes

	trigger = {
		NOT = { has_global_flag = fortitude_initialized_v2point0 }
	}

	mean_time_to_happen = {
		months = 1
	}

	option = {
		name = fortitude.okay
		ai_chance = { factor = 100 }
		set_global_flag = fortitude_initialized_v2point0
		clr_global_flag = fortitude_settings_pulse_fast
		clr_global_flag = fortitude_settings_pulse_slow
		set_global_flag = fortitude_settings_system_forts
		set_global_flag = fortitude_settings_system_buildings
		set_global_flag = fortitude_settings_system_reposition_colonists
		set_global_flag = fortitude_settings_system_abandon_ideas
		set_global_flag = fortitude_settings_allowed
		#the default settings is to not send any notification, for the sake of immersion.
		#for those willing to sacrifice a bit of immersion for the sake of gameplay-related information,
		#it could be interesting to turn on fortitude_notify_abandon_events because those are rare (non-spammy) and important from a gameplay perspective
		every_country = {
			clr_country_flag = fortitude_settings_notify_forts
			clr_country_flag = fortitude_settings_notify_buildings
			clr_country_flag = fortitude_settings_notify_reposition_colonists
			clr_country_flag = fortitude_settings_notify_abandon_ideas
		}
		#make sure pulse events are spread out across countries
		#if they do not happen at the same time, there is even less of a chance of a lag spike
		every_country = {
			limit = {
				ai = yes
			}
			random_list = {
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 15
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 30
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 45
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 60
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 75
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 90
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 105
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 120
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 135
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 150
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 165
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 180
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 195
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 210
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 225
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 240
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 255
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 270
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 285
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 300
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 315
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 330
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 345
					    hidden = yes
					}
				}
				4 = {
					add_country_modifier = { 
					    name = fortitude_pulse_cooldown
					    duration = 360
					    hidden = yes
					}
				}
			}
		}
	}
}



country_event = {
	id = fortitude_events_settings.2
	title = fortitude_events_settings.2.t
	desc = fortitude_events_settings.2.d
	picture = BATTLE_eventPicture
	major = yes
	
	is_triggered_only = yes

	trigger = {
		has_global_flag = fortitude_initialized_v2point0
		ai = no
	}

	option = {
		name = fortitude_events_settings_pulse_makeitfast
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			fortitude_global_any_system_active = yes
			NOT = { has_global_flag = fortitude_settings_pulse_fast }
			NOT = { has_global_flag = fortitude_settings_pulse_slow }
		}
		set_global_flag = fortitude_settings_pulse_fast
		clr_global_flag = fortitude_settings_pulse_slow #for sanity
	}
	option = {
		name = fortitude_events_settings_pulse_makeitmediumfromfast
		trigger = {
			has_global_flag = fortitude_settings_allowed
			fortitude_global_any_system_active = yes
			has_global_flag = fortitude_settings_pulse_fast
		}
		clr_global_flag = fortitude_settings_pulse_fast
		clr_global_flag = fortitude_settings_pulse_slow #for sanity
	}
	option = {
		name = fortitude_events_settings_pulse_makeitmediumfromslow
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			fortitude_global_any_system_active = yes
			has_global_flag = fortitude_settings_pulse_slow
		}
		clr_global_flag = fortitude_settings_pulse_fast #for sanity
		clr_global_flag = fortitude_settings_pulse_slow
	}
	option = {
		name = fortitude_events_settings_pulse_makeitslow
		trigger = {
			has_global_flag = fortitude_settings_allowed
			fortitude_global_any_system_active = yes
			NOT = { has_global_flag = fortitude_settings_pulse_fast }
			NOT = { has_global_flag = fortitude_settings_pulse_slow }
		}
		set_global_flag = fortitude_settings_pulse_slow
		clr_global_flag = fortitude_settings_pulse_fast #for sanity
	}

	option = {
		name = fortitude_events_settings_system_forts_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			NOT = { has_global_flag = fortitude_settings_system_forts }
		}
		set_global_flag = fortitude_settings_system_forts
	}
	option = {
		name = fortitude_events_settings_system_forts_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_forts
		}
		clr_global_flag = fortitude_settings_system_forts
	}
	option = {
		name = fortitude_events_settings_notify_forts_turniton
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_forts
			NOT = { has_country_flag = fortitude_settings_notify_forts }
		}
		set_country_flag = fortitude_settings_notify_forts
	}
	option = {
		name = fortitude_events_settings_notify_forts_turnitoff
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_forts
			has_country_flag = fortitude_settings_notify_forts
		}
		clr_country_flag = fortitude_settings_notify_forts
	}

	option = {
		name = fortitude_events_settings_system_buildings_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			NOT = { has_global_flag = fortitude_settings_system_buildings }
		}
		set_global_flag = fortitude_settings_system_buildings
	}
	option = {
		name = fortitude_events_settings_system_buildings_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_buildings
		}
		clr_global_flag = fortitude_settings_system_buildings
	}
	option = {
		name = fortitude_events_settings_notify_buildings_turniton
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_buildings
			NOT = { has_country_flag = fortitude_settings_notify_buildings }
		}
		set_country_flag = fortitude_settings_notify_buildings
	}
	option = {
		name = fortitude_events_settings_notify_buildings_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_buildings
			has_country_flag = fortitude_settings_notify_buildings
		}
		clr_country_flag = fortitude_settings_notify_buildings
	}

	option = {
		name = fortitude_events_settings_system_reposition_colonists_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			NOT = { has_global_flag = fortitude_settings_system_reposition_colonists }
		}
		set_global_flag = fortitude_settings_system_reposition_colonists
	}
	option = {
		name = fortitude_events_settings_system_reposition_colonists_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_reposition_colonists
		}
		clr_global_flag = fortitude_settings_system_reposition_colonists
	}
	option = {
		name = fortitude_events_settings_notify_reposition_colonists_turniton
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_reposition_colonists
			NOT = { has_country_flag = fortitude_settings_notify_reposition_colonists }
		}
		set_country_flag = fortitude_settings_notify_reposition_colonists
	}
	option = {
		name = fortitude_events_settings_notify_reposition_colonists_turnitoff
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_reposition_colonists
			has_country_flag = fortitude_settings_notify_reposition_colonists
		}
		clr_country_flag = fortitude_settings_notify_reposition_colonists
	}

	option = {
		name = fortitude_events_settings_system_abandon_ideas_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			NOT = { has_global_flag = fortitude_settings_system_abandon_ideas }
		}
		set_global_flag = fortitude_settings_system_abandon_ideas
	}
	option = {
		name = fortitude_events_settings_system_abandon_ideas_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_abandon_ideas
		}
		clr_global_flag = fortitude_settings_system_abandon_ideas
	}
	option = {
		name = fortitude_events_settings_notify_abandon_ideas_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_abandon_ideas
			NOT = { has_country_flag = fortitude_settings_notify_abandon_ideas }
		}
		set_country_flag = fortitude_settings_notify_abandon_ideas
	}
	option = {
		name = fortitude_events_settings_notify_abandon_ideas_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
			has_global_flag = fortitude_settings_system_abandon_ideas
			has_country_flag = fortitude_settings_notify_abandon_ideas
		}
		clr_country_flag = fortitude_settings_notify_abandon_ideas
	}

	option = { #can get here by manually executing this event from the command window
		name = fortitude_events_settings_allowed_turniton
		highlight = yes #would switch to the recommended setting
		trigger = {
			NOT = { has_global_flag = fortitude_settings_allowed }
		}
		set_global_flag = fortitude_settings_allowed
	}
	option = {
		name = fortitude_events_settings_allowed_turnitoff
		trigger = {
			has_global_flag = fortitude_settings_allowed
		}
		#SHUT. DOWN. EVERYTHING.
		#the menu becomes inaccessible and notifications are suppressed
		#the systems the were enabled stay enabled, working silently
		#could be useful in multiplayer to ensure nobody tampers with the settings
		#and that everybody receives the same amount of extra info (none)
		every_country = {
			clr_country_flag = fortitude_settings_notify_forts
			clr_country_flag = fortitude_settings_notify_buildings
			clr_country_flag = fortitude_settings_notify_reposition_colonists
			clr_country_flag = fortitude_settings_notify_abandon_ideas
		}
		clr_global_flag = fortitude_settings_allowed
	}

	option = {
		name = fortitude_events_settings_exit
		ai_chance = { factor = 100 }
	}

	after = {
		if = {
			limit = {
				NOT = { fortitude_global_any_system_active = yes }
			}
			clr_global_flag = fortitude_settings_pulse_fast
	   		set_global_flag = fortitude_settings_pulse_slow
		}
	}
}

#country_event = { #a simple event used to debug/check triggers, etc
#	id = fortitude_events_settings.3
#	title = fortitude_events_settings.3.t
#	desc = fortitude_events_settings.3.d
#	picture = BATTLE_eventPicture
#	major = yes
#	
#	is_triggered_only = yes
#
#	trigger = {
#		A04 = {	fortitude_country_dislikes_prev = yes }
#	}
#
#	option = {
#		name = fortitude.okay
#		ai_chance = { factor = 100 }
#	}
#}

